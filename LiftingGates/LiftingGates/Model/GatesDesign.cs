﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace LiftingGates.Model
{
    /// <summary>
    /// Конструкция гаражных ворот с установленными размерами всех составных элементов
    /// </summary>
    public class GatesDesign
    {
        /// <summary>
        /// Величина козырька - вылет ворот в открытом состоянии за пределы проема
        /// </summary>
        public float VisorValue { get; set; }

        /// <summary>
        /// Наклон козырька относительно горизонта
        /// </summary>
        public float VisorAngle { get; set; }

        public Gates  Gates { get; set; }

        public Rack Rack { get; set; }

        public LowerLever LowerLever { get; set; }

        public UpperLever UpperLever { get; set; }

        public Spring Spring { get; set; }

        public GatesDesign()
        {
            //Сопряжение шарниров рычагов и стоек
            Rack.LowerHinger = LowerLever.MidleHinger;
            Rack.UpperHinger = UpperLever.UpperHinger;
            Gates.UpperHinger = UpperLever.LowerHinger;
            Gates.LowerHinger = LowerLever.EdgeHinger;

            //Задание координат всех шарниров начального положения ворот при закрытом состоянии
            Rack.LowerHinger.XPosition = Rack.UpperHinger.XPosition = 
            Gates.LowerHinger.XPosition = Gates.UpperHinger.XPosition = 0;

        }


        public GatesDesign(float gatesHight, float upperLimit) : this()
        {
            Gates.Hight = gatesHight;
            Rack.UpperHinger.YPosition = upperLimit;

        }

        /// <summary>
        /// На основе желаемого размера козырька 
        /// (вылет ворот в открытом состоянии за пределы проема)
        /// рассчитываются размеры рычагов и расположение шарниров в данном
        /// дизайне ворот
        /// </summary>
        /// <param name="visor"></param>
        public void CalculateGeometry(float visor, float visorAngle)
        {
            VisorValue = visor;
            VisorAngle = visorAngle;
            //Производится расчет на основе двух крайних расположений ворот: вертикальном и горизонтальном
            //...
        }

        /// <summary>
        /// Расчет необходимой величины силы воздействия на нижний рычаг,
        /// для подъема ворот без особых усилий со стороны человека
        /// </summary>
        public void CalculateForces()
        {
            //Заглушка
            Spring.Force = 1.1F;
        }
    }
}
