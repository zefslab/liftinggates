﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiftingGates.Model
{
    /// <summary>
    /// Верхний рычаг
    /// </summary>
    public class UpperLever
    {
        /// <summary>
        /// Верхний шарнир
        /// </summary>
        public Hinge UpperHinger { get; set; }

        /// <summary>
        /// Нижний шарнир
        /// </summary>
        public Hinge LowerHinger { get; set; }

        /// <summary>
        /// Длина рычага
        /// </summary>
        public float Linght { get; set; }
        
    }
}
