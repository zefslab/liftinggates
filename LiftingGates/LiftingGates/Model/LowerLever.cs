﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiftingGates.Model
{
    /// <summary>
    /// Нижний рычаг
    /// </summary>
    public class LowerLever
    {
        /// <summary>
        /// Средний шарнир
        /// </summary>
        public Hinge MidleHinger { get; set; }

        /// <summary>
        /// Крайний шарнир
        /// </summary>
        public Hinge EdgeHinger { get; set; }
       

        /// <summary>
        /// Длина рычага
        /// </summary>
        public float Linght { get; set; }
    }
}
