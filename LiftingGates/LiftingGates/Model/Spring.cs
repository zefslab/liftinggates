﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiftingGates.Model
{
    public class Spring
    {
        /// <summary>
        ///Изменяющаяся сила в Нютонах, в зависимости от удлинения пружины 
        /// </summary>
        public float Force { get; set; }


    }
}
