﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace LiftingGates.Model
{
    /// <summary>
    /// Ворота
    /// </summary>
    public class Gates
    {
        public float Hight { get; set; }

        public float Thickness { get; set; }

        public float Width { get; set; }

        public float Weight { get; set; }
        
        /// <summary>
        /// Верхний шарнир
        /// </summary>
        public Hinge UpperHinger { get; set; }

        /// <summary>
        /// Нижний шарнир
        /// </summary>
        public Hinge LowerHinger { get; set; }

        /// <summary>
        /// Настраиваемое вертикальное положение верхнего шарнира относительно верха ворот
        /// </summary>
        public float VarablePositionUpperHinger
        {
            set { UpperHinger.YPosition = Hight-value; }
        }
    }
}
