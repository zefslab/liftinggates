﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiftingGates.Model
{
    /// <summary>
    /// Стойка
    /// </summary>
    public class Rack
    {
        /// <summary>
        /// Верхний шарнир
        /// </summary>
        public Hinge UpperHinger { get; set; }

        /// <summary>
        /// Нижний шарнир
        /// </summary>
        public Hinge LowerHinger { get; set; }

        /// <summary>
        /// Высота стойки
        /// </summary>
        public float Hight { get; set; }

        /// <summary>
        /// Настраиваемое вертикальное положение нижнего шарнира
        /// </summary>
        public float VarablePositionLowerHinger
        {
            set { LowerHinger.YPosition = value; }
        }
        
    }
}
