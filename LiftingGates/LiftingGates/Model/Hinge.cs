﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiftingGates.Model
{
    /// <summary>
    /// Шарнир
    /// </summary>
    public class Hinge
    {
        /// <summary>
        /// Координата шарнира в пространстве в определенном положении рычага
        /// </summary>
        public float XPosition { get; set; }
        public float YPosition { get; set; }
    }
}
